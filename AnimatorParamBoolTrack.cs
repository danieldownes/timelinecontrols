using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

[TrackColor(0.255f, 0.37f, 0.9623f)]
[TrackClipType(typeof(AnimatorParamBoolClip))]
[TrackBindingType(typeof(Animator))]
public class AnimatorParamBoolTrack : TrackAsset
{
    public override Playable CreateTrackMixer(PlayableGraph graph, GameObject go, int inputCount)
    {
        return ScriptPlayable<AnimatorParamBoolMixerBehaviour>.Create(graph, inputCount);
    }
}
