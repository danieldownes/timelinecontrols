using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

[Serializable]
public class AnimatorParamBoolClip : PlayableAsset, ITimelineClipAsset
{
    public AnimatorParamBoolBehaviour template = new AnimatorParamBoolBehaviour();

    public ClipCaps clipCaps
    {
        get { return ClipCaps.None; }
    }

    public override Playable CreatePlayable(PlayableGraph graph, GameObject owner)
    {
        var playable = ScriptPlayable<AnimatorParamBoolBehaviour>.Create(graph, template);
        AnimatorParamBoolBehaviour clone = playable.GetBehaviour();
        return playable;
    }
}
