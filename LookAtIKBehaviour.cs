using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

[Serializable]
public class LookAtIKBehaviour : PlayableBehaviour
{
    public bool lookAtCamera = false;
    public Transform target;
    public HeadController.Priority priority = HeadController.Priority.HIGH;
    [Range(0, 1)] public float weight = 1f;
    [Range(0, 1)] public float smoothTime = 0.2f;

    public override void OnGraphStart(Playable playable)
    {

    }
}
