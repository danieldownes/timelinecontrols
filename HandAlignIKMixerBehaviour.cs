using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using RootMotion.FinalIK;

public class HandAlignIKMixerBehaviour : PlayableBehaviour
{
    // NOTE: This function is called at runtime and edit time.  Keep that in mind when setting the values of properties.
    public override void ProcessFrame(Playable playable, FrameData info, object playerData)
    {
        FullBodyBipedIK trackBinding = playerData as FullBodyBipedIK;

        if (!trackBinding)
            return;

        int inputCount = playable.GetInputCount();
        float posWeightResult = 0;
        float rotWeightResult = 0;

        HandAlignIKBehaviour selectedInput = null;
        float highestWeight = 0;

        for (int i = 0; i < inputCount; i++)
        {
            float inputWeight = playable.GetInputWeight(i);
            ScriptPlayable<HandAlignIKBehaviour> inputPlayable = (ScriptPlayable<HandAlignIKBehaviour>)playable.GetInput(i);
            HandAlignIKBehaviour input = inputPlayable.GetBehaviour();

            posWeightResult += input.positionWeight * inputWeight;
            rotWeightResult += input.rotationWeight * inputWeight;

            if (inputWeight > highestWeight)
            {
                selectedInput = input;
                highestWeight = inputWeight;
            }
        }

        if (selectedInput != null)
        {
            IKEffector effector = null;

            if (selectedInput.hand == HandAlignIKBehaviour.Side.LEFT)
                effector = trackBinding.solver.leftHandEffector;
            else
                effector = trackBinding.solver.rightHandEffector;

            effector.positionWeight = posWeightResult;
            effector.rotationWeight = rotWeightResult;
            effector.target = selectedInput.target;
        }
    }
}
