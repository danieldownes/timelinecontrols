using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

[Serializable]
public class LookAtIKClip : PlayableAsset, ITimelineClipAsset
{
    public LookAtIKBehaviour template = new LookAtIKBehaviour();
    public ExposedReference<Transform> target;

    public ClipCaps clipCaps
    {
        get { return ClipCaps.Blending; }
    }

    public override Playable CreatePlayable(PlayableGraph graph, GameObject owner)
    {
        var playable = ScriptPlayable<LookAtIKBehaviour>.Create(graph, template);
        LookAtIKBehaviour clone = playable.GetBehaviour();
        clone.target = target.Resolve(graph.GetResolver());
        return playable;
    }
}
