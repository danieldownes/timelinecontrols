using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using RootMotion.FinalIK;

[Serializable]
public class HandAlignIKClip : PlayableAsset, ITimelineClipAsset
{
    public HandAlignIKBehaviour template = new HandAlignIKBehaviour();
    public ExposedReference<Transform> target;

    public ClipCaps clipCaps
    {
        get { return ClipCaps.Blending; }
    }

    public override Playable CreatePlayable(PlayableGraph graph, GameObject owner)
    {
        var playable = ScriptPlayable<HandAlignIKBehaviour>.Create(graph, template);
        HandAlignIKBehaviour clone = playable.GetBehaviour();
        clone.target = target.Resolve(graph.GetResolver());
        return playable;
    }
}
