using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

public class AnimatorParamTriggerMixerBehaviour : PlayableBehaviour
{
    
    public TimelineClip[] _clips;

    public AnimatorParamTriggerBehaviour activeInput;

    public override void OnBehaviourPlay(Playable playable, FrameData info)
    {
        base.OnBehaviourPlay(playable, info);
        activeInput = null;
    }

    public override void ProcessFrame(Playable playable, FrameData info, object playerData)
    {
        int inputCount = playable.GetInputCount ();
        double time = playable.GetGraph().GetRootPlayable(0).GetTime();
        Animator trackBinding = playerData as Animator;

        if (!trackBinding)
            return;
        
        for (int i = 0; i < inputCount; i++)
        {
            ScriptPlayable<AnimatorParamTriggerBehaviour> inputPlayable = (ScriptPlayable<AnimatorParamTriggerBehaviour>)playable.GetInput(i);
            AnimatorParamTriggerBehaviour input = inputPlayable.GetBehaviour ();
                   
            if (input.CheckForTrigger(time, _clips[i].start)){
                trackBinding.SetTrigger(input.parameter);
            }
        }
    }
}
