using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using RootMotion.FinalIK;

[TrackColor(0f, 0.4866645f, 1f)]
[TrackClipType(typeof(LookAtIKClip))]
[TrackBindingType(typeof(LookAtIK))]
public class LookAtIKTrack : TrackAsset
{
    public override Playable CreateTrackMixer(PlayableGraph graph, GameObject go, int inputCount)
    {
        return ScriptPlayable<LookAtIKMixerBehaviour>.Create(graph, inputCount);
    }
}
