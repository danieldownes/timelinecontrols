using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using RogoDigital.Lipsync;

public class LipSyncPlayMixerBehaviour : PlayableBehaviour
{
    public TimelineClip[] _clips;
    public TimelineClip currentClip;
    public LipSyncPlayBehaviour currentInput;


    public override void ProcessFrame(Playable playable, FrameData info, object playerData)
    {
        LipSync trackBinding = playerData as LipSync;
        double time = playable.GetGraph().GetRootPlayable(0).GetTime();
        int inputCount = playable.GetInputCount();
        int numWithin = 0;

        if (!trackBinding)
            return;

        for (int i = 0; i < inputCount; i++)
        {
            ScriptPlayable<LipSyncPlayBehaviour> inputPlayable = (ScriptPlayable<LipSyncPlayBehaviour>)playable.GetInput(i);
            LipSyncPlayBehaviour input = inputPlayable.GetBehaviour();

            var startTime = _clips[i].start;
            var endTime = _clips[i].end;

            var startTrigger = input.CheckForClipStart(time, _clips[i].start);
            var endTrigger = input.CheckForClipEnd(time, _clips[i].end);
            bool isWithin = input.TimeIsWithinClip(time, startTime, endTime);

            if (isWithin)
                numWithin++;

            if (startTrigger && input.lipSyncData)
            {
                trackBinding.Play(input.lipSyncData);
                currentClip = _clips[i];
                currentInput = input;
            }

            if (input.stopAtClipEnd && trackBinding.IsPlaying && endTrigger)
                trackBinding.Stop(true);
        }

        if (numWithin == 0 && trackBinding.IsPlaying && (currentInput != null && currentInput.stopAtClipEnd))
            trackBinding.Stop(true);

    }
}