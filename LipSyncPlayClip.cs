using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

[Serializable]
public class LipSyncPlayClip : PlayableAsset, ITimelineClipAsset
{
    public LipSyncPlayBehaviour template = new LipSyncPlayBehaviour();

    public ClipCaps clipCaps
    {
        get { return ClipCaps.None; }
    }

    public override Playable CreatePlayable(PlayableGraph graph, GameObject owner)
    {
        var playable = ScriptPlayable<LipSyncPlayBehaviour>.Create(graph, template);
        LipSyncPlayBehaviour clone = playable.GetBehaviour();
        return playable;
    }
}