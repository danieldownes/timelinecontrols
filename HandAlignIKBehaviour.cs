using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using RootMotion.FinalIK;

[Serializable]
public class HandAlignIKBehaviour : PlayableBehaviour
{
    public enum Side { LEFT, RIGHT }

    public Transform target;
    public float positionWeight;
    public float rotationWeight;
    public Side hand;

    public override void OnGraphStart(Playable playable)
    {

    }
}
