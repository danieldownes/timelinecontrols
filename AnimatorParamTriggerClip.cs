using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

[Serializable]
public class AnimatorParamTriggerClip : PlayableAsset, ITimelineClipAsset
{
    public AnimatorParamTriggerBehaviour template = new AnimatorParamTriggerBehaviour ();

    public ClipCaps clipCaps
    {
        get { return ClipCaps.None; }
    }

    public override Playable CreatePlayable (PlayableGraph graph, GameObject owner)
    {
        var playable = ScriptPlayable<AnimatorParamTriggerBehaviour>.Create (graph, template);
        AnimatorParamTriggerBehaviour clone = playable.GetBehaviour ();
        return playable;
    }
}
