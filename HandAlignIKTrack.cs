using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using RootMotion.FinalIK;

[TrackColor(0.855f, 0.8623f, 0.87f)]
[TrackClipType(typeof(HandAlignIKClip))]
[TrackBindingType(typeof(FullBodyBipedIK))]
public class HandAlignIKTrack : TrackAsset
{
    public override Playable CreateTrackMixer(PlayableGraph graph, GameObject go, int inputCount)
    {
        return ScriptPlayable<HandAlignIKMixerBehaviour>.Create(graph, inputCount);
    }
}
