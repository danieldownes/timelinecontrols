using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using RootMotion.FinalIK;

public class LookAtIKMixerBehaviour : PlayableBehaviour
{
    Vector3 vel;
    Camera sceneCam;
    HeadController headController;

    // NOTE: This function is called at runtime and edit time.  Keep that in mind when setting the values of properties.
    public override void ProcessFrame(Playable playable, FrameData info, object playerData)
    {
        LookAtIK trackBinding = playerData as LookAtIK;
        Transform trackBindingTransform = trackBinding.solver.target;

        if (!trackBinding)
            return;

        int inputCount = playable.GetInputCount();

        Vector3 resultPosition = Vector3.zero;
        Vector3 finalPosition = Vector3.zero;
        float resultWeight = 0;
        float smoothTime = 0;

        if (!sceneCam)
        {
            sceneCam = Camera.main;

            if (!sceneCam)
                sceneCam = GameObject.FindObjectOfType<Camera>();
        }

        var highestWeight = 0f;
        LookAtIKBehaviour highestInput = null;

        for (int i = 0; i < inputCount; i++)
        {
            float inputWeight = playable.GetInputWeight(i);
            ScriptPlayable<LookAtIKBehaviour> inputPlayable = (ScriptPlayable<LookAtIKBehaviour>)playable.GetInput(i);
            LookAtIKBehaviour input = inputPlayable.GetBehaviour();

            Transform target = input.target;
            if (input.lookAtCamera && sceneCam)
                target = sceneCam.transform;

            if (inputWeight > highestWeight)
            {
                highestWeight = inputWeight;
                highestInput = input;
                finalPosition = target.position;
            }

            if (target)
            {
                resultPosition += target.position * inputWeight;
                resultWeight += input.weight * inputWeight;
                smoothTime += input.smoothTime * inputWeight;
            }
        }

        if (headController == null)
            headController = trackBinding.GetComponent<HeadController>();

        if (headController)   // use the head controller if present
        {
            headController.SetTarget(finalPosition, highestInput.priority);
            headController.SetWeight(resultWeight, highestInput.priority);
        }
        else
        {
            // directly use the LookAtIk if no controller is present
            trackBindingTransform.position = Vector3.SmoothDamp(trackBindingTransform.position, resultPosition, ref vel, smoothTime, 100);
            trackBinding.solver.SetLookAtWeight(resultWeight);
        }
    }
}
