using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

[TrackColor(0.255f, 0.8623f, 0.87f)]
[TrackClipType(typeof(AnimatorParamTriggerClip))]
[TrackBindingType(typeof(Animator))]
public class AnimatorParamTriggerTrack : TrackAsset
{
    public override Playable CreateTrackMixer(PlayableGraph graph, GameObject go, int inputCount)
    {
        var result = ScriptPlayable<AnimatorParamTriggerMixerBehaviour>.Create(graph, inputCount);
        result.GetBehaviour()._clips = GetClips() as TimelineClip[];
        return result;
    }
}
