using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using RogoDigital.Lipsync;

[TrackColor(0.855f, 0.8623f, 0.87f)]
[TrackClipType(typeof(LipSyncPlayClip))]
[TrackBindingType(typeof(LipSync))]
public class LipSyncPlayTrack : TrackAsset
{
    public override Playable CreateTrackMixer(PlayableGraph graph, GameObject go, int inputCount)
    {
        var result = ScriptPlayable<LipSyncPlayMixerBehaviour>.Create(graph, inputCount);
        result.GetBehaviour()._clips = GetClips() as TimelineClip[];
        return result;
    }
}
