using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using RogoDigital.Lipsync;

[Serializable]
public class LipSyncPlayBehaviour : PlayableBehaviour
{
    public LipSyncData lipSyncData;
    public bool stopAtClipEnd = false;

    double _lastTime0 = -1;
    public bool CheckForClipStart(double currentTime, double startTime)
    {
        bool doTrigger = false;

        if (Application.isPlaying)
        {
            if (_lastTime0 < startTime && currentTime >= startTime)
            {
                doTrigger = true;
            }
            _lastTime0 = currentTime;
        }

        return doTrigger;
    }

    double _lastTime1 = -1;
    public bool CheckForClipEnd(double currentTime, double endTime)
    {
        bool doTrigger = false;

        if (Application.isPlaying)
        {
            if (_lastTime1 < endTime && currentTime >= endTime)
                doTrigger = true;

            _lastTime1 = currentTime;
        }

        return doTrigger;
    }

    public bool TimeIsWithinClip(double time, double startTime, double endTime)
    {
        return time > startTime
            && time < endTime;
    }

    public override void OnGraphStart(Playable playable)
    {

    }
}
