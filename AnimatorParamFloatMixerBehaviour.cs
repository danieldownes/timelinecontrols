using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

public class AnimatorParamFloatMixerBehaviour : PlayableBehaviour
{
    // NOTE: This function is called at runtime and edit time.  Keep that in mind when setting the values of properties.
    public override void ProcessFrame(Playable playable, FrameData info, object playerData)
    {
        Animator trackBinding = playerData as Animator;

        if (!trackBinding)
            return;

        int inputCount = playable.GetInputCount();

        float result = 0;
        float highestWeight = 0;
        AnimatorParamFloatBehaviour selectedInput = null;

        for (int i = 0; i < inputCount; i++)
        {
            float inputWeight = playable.GetInputWeight(i);
            ScriptPlayable<AnimatorParamFloatBehaviour> inputPlayable = (ScriptPlayable<AnimatorParamFloatBehaviour>)playable.GetInput(i);
            AnimatorParamFloatBehaviour input = inputPlayable.GetBehaviour();

            // Use the above variables to process each frame of this playable.

            result += input.value * inputWeight;

            if (inputWeight > highestWeight)
            {
                highestWeight = inputWeight;
                selectedInput = input;
            }
        }

        if (selectedInput != null)
        {
            trackBinding.SetFloat(selectedInput.parameter, result);
        }
    }
}
