using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

[Serializable]
public class AnimatorParamFloatClip : PlayableAsset, ITimelineClipAsset
{
    public AnimatorParamFloatBehaviour template = new AnimatorParamFloatBehaviour();

    public ClipCaps clipCaps
    {
        get { return ClipCaps.Blending; }
    }

    public override Playable CreatePlayable(PlayableGraph graph, GameObject owner)
    {
        var playable = ScriptPlayable<AnimatorParamFloatBehaviour>.Create(graph, template);
        AnimatorParamFloatBehaviour clone = playable.GetBehaviour();
        return playable;
    }
}
