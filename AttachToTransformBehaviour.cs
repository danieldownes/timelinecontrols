using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

[Serializable]
public class AttachToTransformBehaviour : PlayableBehaviour
{
    public Transform target;
    public bool interpolate = true;
    [Range(0,1)] public float smoothTime = 0.1f;
    [Range(0,1)] public float rotationSlerp = 0.15f;
    public Vector3 tweakRotation = Vector3.zero;

    public override void OnGraphStart (Playable playable)
    {
        
    }
}
