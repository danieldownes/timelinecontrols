using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

[Serializable]
public class AttachToTransformClip : PlayableAsset, ITimelineClipAsset
{
    public AttachToTransformBehaviour template = new AttachToTransformBehaviour ();
    public ExposedReference<Transform> target;

    public ClipCaps clipCaps
    {
        get { return ClipCaps.None; }
    }

    public override Playable CreatePlayable (PlayableGraph graph, GameObject owner)
    {
        var playable = ScriptPlayable<AttachToTransformBehaviour>.Create (graph, template);
        AttachToTransformBehaviour clone = playable.GetBehaviour ();
        clone.target = target.Resolve (graph.GetResolver ());
        return playable;
    }
}
