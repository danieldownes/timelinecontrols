using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

[Serializable]
public class AnimatorParamFloatBehaviour : PlayableBehaviour
{
    public float value = 0;
    public string parameter = "param id";

    public override void OnGraphStart(Playable playable)
    {

    }
}
