using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

[Serializable]
public class AnimatorParamTriggerBehaviour : PlayableBehaviour
{
    public string parameter;

    double _lastTime = -1;

    public bool CheckForTrigger(double currentTime, double startTime)
    {
        bool doTrigger = false;

        if (Application.isPlaying)
        {
            if (_lastTime < startTime && currentTime >= startTime)
                doTrigger = true;

            _lastTime = currentTime;
        }

        return doTrigger;
    }

    public override void OnGraphStart(Playable playable)
    {
    }
}
