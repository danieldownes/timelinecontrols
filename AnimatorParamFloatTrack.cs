using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

[TrackColor(0.255f, 0.9623f, 0.37f)]
[TrackClipType(typeof(AnimatorParamFloatClip))]
[TrackBindingType(typeof(Animator))]
public class AnimatorParamFloatTrack : TrackAsset
{
    public override Playable CreateTrackMixer(PlayableGraph graph, GameObject go, int inputCount)
    {
        return ScriptPlayable<AnimatorParamFloatMixerBehaviour>.Create (graph, inputCount);
    }
}
