using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

public class AttachToTransformMixerBehaviour : PlayableBehaviour
{
    Vector3 vel;

    public override void ProcessFrame(Playable playable, FrameData info, object playerData)
    {
        Transform trackBinding = playerData as Transform;

        if (!trackBinding)
            return;

        int inputCount = playable.GetInputCount();
        float highestWeight = 0;
        AttachToTransformBehaviour selectedInput = null;

        for (int i = 0; i < inputCount; i++)
        {
            float inputWeight = playable.GetInputWeight(i);
            ScriptPlayable<AttachToTransformBehaviour> inputPlayable = (ScriptPlayable<AttachToTransformBehaviour>)playable.GetInput(i);
            AttachToTransformBehaviour input = inputPlayable.GetBehaviour();

            if (inputWeight > highestWeight)
            {
                selectedInput = input;
                highestWeight = inputWeight;
            }
        }

        if (trackBinding && selectedInput != null)
        {

            // attach the object to the new parent
            if (trackBinding.parent != selectedInput.target)
                trackBinding.parent = selectedInput.target;

            // set the position if there is no blending or if the app is in editor preview mode
            if (selectedInput.smoothTime == 0 || !Application.isPlaying || !selectedInput.interpolate)
                trackBinding.localPosition = Vector3.zero;

            // set the rotation if there is no blending or if the app is in editor preview mode            
            if (selectedInput.rotationSlerp == 1 || !Application.isPlaying || !selectedInput.interpolate)
                trackBinding.localEulerAngles = selectedInput.tweakRotation;

            if (selectedInput.interpolate && Application.isPlaying)
            {

                // smooth inerpolate the position
                if (selectedInput.smoothTime > 0)
                    trackBinding.position = Vector3.SmoothDamp(trackBinding.position, selectedInput.target.position, ref vel, selectedInput.smoothTime);

                // store current rotation
                var currentRotation = trackBinding.rotation;

                // set the target rotation
                trackBinding.localEulerAngles = selectedInput.tweakRotation;

                // get the world target rotation
                var targetRotation = trackBinding.rotation;

                // slerp toward the target rotation 
                if (selectedInput.rotationSlerp < 1)
                    trackBinding.rotation = Quaternion.Slerp(currentRotation, targetRotation, selectedInput.rotationSlerp);
            }
        }
    }
}
