using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

public class AnimatorParamBoolMixerBehaviour : PlayableBehaviour
{
    public override void ProcessFrame(Playable playable, FrameData info, object playerData)
    {
        Animator trackBinding = playerData as Animator;

        if (!trackBinding)
            return;

        int inputCount = playable.GetInputCount();

        float highestWeight = 0;
        AnimatorParamBoolBehaviour selectedInput = null;

        for (int i = 0; i < inputCount; i++)
        {
            float inputWeight = playable.GetInputWeight(i);
            ScriptPlayable<AnimatorParamBoolBehaviour> inputPlayable = (ScriptPlayable<AnimatorParamBoolBehaviour>)playable.GetInput(i);
            AnimatorParamBoolBehaviour input = inputPlayable.GetBehaviour();

            if (inputWeight > highestWeight)
            {
                highestWeight = inputWeight;
                selectedInput = input;
            }
        }

        if (selectedInput != null)
        {
            trackBinding.SetBool(selectedInput.parameter, selectedInput.value);
        }
    }
}
